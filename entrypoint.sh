#!/bin/bash
set -e


# instalar apache2 y git
# cada vez que se inicie el contendor
apt update && apt install apache2 git -y



if test -d /app; then

	# Esto se ejecuta siempre que la condicion sea verdadera
	cd /app && git pull

else

	# esto se ejecuta siempre que la condicion sea falsa
	git clone https://gitlab.com/sergio.pernas1/product.git /app 

fi

# definir en que rama se debe desplegar la app

# testing develop main

cd /app && git checkout $BRANCHAPP

cp /app/apache2.conf /etc/apache2/
cp /app/000-default.conf /etc/apache2/sites-available/

# Se ejecuta todo lo que se haya definido
# en el Dockerfile bajo la instruccion 'CMD'
exec "$@"

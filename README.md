### README.md

# Proyecto Dockerizado con Apache y Git

Este repositorio contiene una configuración para desplegar una aplicación web utilizando Docker, Apache2 y Git. La aplicación se obtiene desde un repositorio Git y se despliega en un servidor Apache dentro de un contenedor Docker.

## Archivos incluidos

### Dockerfile
El `Dockerfile` se encarga de construir la imagen Docker basada en Ubuntu, instalar y configurar Apache, y definir el entrypoint y comando por defecto para iniciar el servidor.

```Dockerfile
FROM ubuntu
EXPOSE 80 443
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2ctl", "-D", "FOREGROUND"]
```

### apache2.conf
Configuración principal de Apache, ajustada para servir la aplicación desde `/app/html`.

```apache2.conf
# Modificación relevante para el despliegue de la app
<Directory /app/html/>
    Options Indexes FollowSymLinks
    AllowOverride None
    Require all granted
</Directory>
```

### 000-default.conf
Configuración del host virtual de Apache, estableciendo el documento raíz a `/app/html`.

```000-default.conf
<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot /app/html
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

### entrypoint.sh
Script de entrada que actualiza el sistema, instala Apache y Git, configura Apache y gestiona el repositorio Git de la aplicación.

```bash
#!/bin/bash
set -e
apt update && apt install apache2 git -y
cp apache2.conf /etc/apache2/
cp 000-default.conf /etc/apache2/sites-available/
if test -d /app; then
    cd ./app && git pull
else
    git clone https://gitlab.com/sergio.pernas1/product.git /app 
fi
cd /app && git checkout $BRANCHAPP
exec "$@"
```

## Construcción de la Imagen Docker

Para construir la imagen Docker, utiliza el siguiente comando en la raíz del proyecto:

```sh
docker build -t nombre_de_tu_imagen .
```

## Lanzamiento del Contenedor

Para lanzar el contenedor, necesitas mapear los puertos según el entorno deseado. Aquí están los comandos para cada entorno:

### Main (puerto 80)

```sh
docker run -d -p 80:80 -e BRANCHAPP=main nombre_de_tu_imagen
```

### Testing (puerto 8000)

```sh
docker run -d -p 8000:80 -e BRANCHAPP=testing nombre_de_tu_imagen
```

### Develop (puerto 8080)

```sh
docker run -d -p 8080:80 -e BRANCHAPP=develop nombre_de_tu_imagen
```

## Notas Adicionales

- Asegúrate de que la variable de entorno `BRANCHAPP` esté establecida correctamente para seleccionar la rama adecuada del repositorio Git.
- El script `entrypoint.sh` se encarga de clonar o actualizar el repositorio Git y de cambiar a la rama especificada.
- El puerto expuesto por el contenedor siempre es el 80, pero se puede mapear a diferentes puertos en el host según sea necesario.

## Contacto

Para cualquier duda o problema, por favor contacta a [webmaster@localhost](mailto:webmaster@localhost).

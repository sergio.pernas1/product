# imagen base
FROM ubuntu


# Abre puertos del contenedor
EXPOSE 80 443

# se copia el entrypoint.sh al raiz del contenedor
COPY entrypoint.sh /entrypoint.sh

# permisos de ejecucion sobre el script
RUN chmod +x /entrypoint.sh

# indicar cual es el entrypoint
ENTRYPOINT ["/entrypoint.sh"]

# los argumentos que se le pasan al script entrypoint.sh
CMD ["apache2ctl", "-D", "FOREGROUND"] 

